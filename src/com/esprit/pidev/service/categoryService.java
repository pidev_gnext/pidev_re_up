/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.esprit.pidev.service;

import com.esprit.pidev.entity.Category;
 import com.esprit.pidev.util.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author rohben
 */
public class categoryService implements IserviceCategory{
     private Connection cnx;
    private Statement st;
    private PreparedStatement pst;

    public categoryService() {
                        cnx = DataSource.getInstance();

    }
    
    @Override
    public void insert(Category c) {
      
          String requete="INSERT INTO `articlecategory`( `title`, `description`) VALUES ('"+c.getTitle()+"','"+c.getDescription()+"')";
          try {
              st=cnx.createStatement();
              st.executeUpdate(requete);
          } catch (SQLException ex) {
              Logger.getLogger(categoryService.class.getName()).log(Level.SEVERE, null, ex);
          }
    
    }

    @Override
    public void delete(int id) {
       
      String requete="delete from `articlecategory` where id ="+id;
   
          try {
              st=cnx.createStatement();
              st.executeUpdate(requete);
          } catch (SQLException ex) {
              Logger.getLogger(categoryService.class.getName()).log(Level.SEVERE, null, ex);
          }
    }

    @Override
    public void update(Category c, int id) {
   String requete="UPDATE `articlecategory` SET `title`='"+c.getTitle()+"',`description`='"+c.getDescription()+"' WHERE id="+id;
          try {
              st=cnx.createStatement();
              st.executeUpdate(requete);
          } catch (SQLException ex) {
              Logger.getLogger(categoryService.class.getName()).log(Level.SEVERE, null, ex);
          }      

    }

    @Override
    public List<Category> displayall() {


 List<Category> list=new ArrayList<>();

           String requete="select * from articlecategory";
        ResultSet rs;
          try {
              st=cnx.createStatement();
               rs=st.executeQuery(requete);
             while(rs.next()){
            Category p=new Category(rs.getInt(1), rs.getString(2)
                    , rs.getString(3));
              list.add(p);
        }
          } catch (SQLException ex) {
              Logger.getLogger(categoryService.class.getName()).log(Level.SEVERE, null, ex);
          }
       
        return list;



    }
    
}
