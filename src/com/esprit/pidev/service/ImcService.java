package com.esprit.pidev.service;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.sql.ResultSet;
import java.util.List;


import com.esprit.pidev.util.DataSource;
import com.esprit.pidev.entity.Imc;
public class ImcService {
    
	PreparedStatement pst;
	Connection cnx;
	List<Imc> liste =new ArrayList<Imc>(); 
	public ImcService(){
            cnx = DataSource.getInstance();

	}
	public double calculerImc(){
		
		return 0;
	}
	public void addNewImc(Imc imcInstance){
		String req = "INSERT INTO imc (sexe,taille,poid,age,resultat,patient_id)"+
				"VALUES(?,?,?,?,?,?)";
		try {
			pst = cnx.prepareStatement(req);
			pst.setString(1, imcInstance.getSexe());
			pst.setFloat(2, imcInstance.getPoid());
			pst.setFloat(3, imcInstance.getTaille());
			pst.setInt(4, imcInstance.getAge());
			pst.setFloat(5, imcInstance.getResultat());
			pst.setInt(6,imcInstance.getPatient_id());
			pst.executeUpdate();
			System.out.print("done");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	public ArrayList getAllImc(int id){
		String req = "SELECT * FROM imc WHERE patient_id = ? ";
		try {
			pst = cnx.prepareStatement(req);
			pst.setInt(1, id);
			ResultSet rs = pst.executeQuery();
			while(rs.next()){
				Imc imcInstance = new Imc();
				imcInstance.setId(rs.getInt("id"));
				imcInstance.setAge(rs.getInt("age"));
				imcInstance.setPatient_id(id);
				imcInstance.setPoid(rs.getFloat("poid"));
				imcInstance.setResultat(rs.getFloat("result"));
				imcInstance.setTaille(rs.getFloat("taille"));
				imcInstance.setSexe(rs.getString("sexe"));
				liste.add(imcInstance);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return (ArrayList) liste;
	}
}
