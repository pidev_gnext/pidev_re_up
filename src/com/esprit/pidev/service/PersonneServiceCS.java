/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.esprit.pidev.service;

import com.esprit.pidev.entity.PersonneCS;
import com.esprit.pidev.util.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Manel
 */
public class PersonneServiceCS {
         Connection cnx;

    public PersonneServiceCS() {
      cnx = DataSource.getInstance();
    }
       
      public void insertPersonne(PersonneCS p) {
        String requete = "INSERT INTO personne (nom,prenom) VALUES (?,?)";
        try {
            PreparedStatement st = cnx.prepareStatement(requete);
            st.setString(1, p.getNom());
            st.setString(2, p.getPrenom());
            st.executeUpdate();
            System.out.println("Insertion effectué");
        } catch (SQLException ex) {
            //ex.printStackTrace();
            System.err.println("Erreur d'ajout");
        }
    }

    public void updatePersonne(PersonneCS p, int id) {
        String requete = "UPDATE personne set nom=?,prenom=? WHERE id=" + id;
        try {
            PreparedStatement st = cnx.prepareStatement(requete);
            st.setString(1, p.getNom());
            st.setString(2, p.getPrenom());
            st.executeUpdate();
            System.out.println("Modification effectué");
        } catch (SQLException ex) {
            System.err.println("Erreur de la modification");
        }
    }

    public void deletePersonne(PersonneCS p) {
        String requete = "DELETE FROM personne WHERE id=?";
        try {
            PreparedStatement st = cnx.prepareStatement(requete);
            st.setInt(1, p.getId());
            st.executeUpdate();
            System.out.println("Suppression effectué");
        } catch (SQLException ex) {
            System.err.println("Erreur de la suppression");
        }
    }

    public PersonneCS findPersonneById(int id) {
        String requete = "SELECT * FROM personne WHERE id=" + id;
        PersonneCS p = null;
        try {
            Statement st = cnx.createStatement();
            ResultSet rs = st.executeQuery(requete);
            p = new PersonneCS();
            while (rs.next()) {
                p.setId(rs.getInt(1));
                p.setNom(rs.getString(2));
                p.setPrenom(rs.getString(3));
            }
        } catch (SQLException ex) {
            System.err.println("Erreur recherche");
        }
        return p;
    }

    public List<PersonneCS> afficherPersonnes() {
        List<PersonneCS> maListe = new ArrayList<>();
        String requete = "SELECT * FROM personne";
        Statement st;
        try {
            st = cnx.createStatement();
            ResultSet rs = st.executeQuery(requete);
            while (rs.next()) {
                PersonneCS p = new PersonneCS();
                p.setId(rs.getInt(1));
                p.setNom(rs.getString(2));
                p.setPrenom(rs.getString(3));
                maListe.add(p);
            }
            return maListe;
            }catch (SQLException ex) {
            System.err.println("Erreur d'affichage de la liste");
        }
        return null;
        }
}
