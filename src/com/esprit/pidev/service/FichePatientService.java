package com.esprit.pidev.service;
import com.esprit.pidev.util.DataSource;
import com.esprit.pidev.entity.FichePatient;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
public class FichePatientService {
	PreparedStatement pst;
	Connection cnx;
	List<FichePatient> liste =new ArrayList<FichePatient>(); 
	public FichePatientService(){
		cnx = DataSource.getInstance();
         
	}
	public void addNewFichePatient(FichePatient fiche){
		String req = "INSERT INTO fiche_patient "+
				"(content,patient_id) VALUES(?,?)"+
				"";
		try {
			pst = cnx.prepareStatement(req);
			pst.setString(1, fiche.getContent());
			pst.setInt(2, fiche.getPatient_id());
			pst.executeUpdate();
			System.out.println("done");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	public void getOneFicheForOnePatient(int id,FichePatient fiche){
		String req = "SELECT * FROM fiche_patient "+
				//"INNER JOIN patient ON fiche_patient.patient_id = patient.id "+
				//"INNER JOIN user ON patient.user_id = user.id "+
				"WHERE fiche_patient.id = ?";
		try {
			pst = cnx.prepareStatement(req);
			pst.setInt(1, id);
			ResultSet rs = 	pst.executeQuery();
			while(rs.next()){
				fiche.setId(rs.getInt("id"));
				fiche.setDate(rs.getDate("date"));
				fiche.setContent(rs.getString("content"));
				fiche.setPatient_id(rs.getInt("patient_id"));
				System.out.print(rs.getInt("id"));
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	public ArrayList getAllFichesForOneClient(int id){
		String req = "SELECT * FROM fiche_patient WHERE patient_id = ? ";
		try {
			pst = cnx.prepareStatement(req);
			pst.setInt(1,id);
			ResultSet rs = pst.executeQuery();
			while(rs.next()){
				FichePatient fiche = new FichePatient();
				fiche.setContent(rs.getString("content"));
				fiche.setId(rs.getInt("id"));
				fiche.setDate(rs.getDate("date"));
				fiche.setPatient_id(id);
				liste.add(fiche);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return (ArrayList) liste;
	}
	public void editOneFiche(FichePatient fiche){
		String req = "UPDATE fiche_patient set content = ? WHERE id = ? ";
		try {
			pst = cnx.prepareStatement(req);
			pst.setString(1,fiche.getContent());
			pst.setInt(2, fiche.getId());
			pst.executeUpdate();
			System.out.print("updated");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	public void deleteOneFiche(int id){
		String req = "DELETE FROM fiche_patient WHERE  id = ? ";
		try {
			pst = cnx.prepareStatement(req);
			pst.setInt(1,id);
			pst.executeUpdate();
			System.out.print("deleted");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
