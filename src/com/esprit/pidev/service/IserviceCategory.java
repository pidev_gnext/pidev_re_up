/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.esprit.pidev.service;


import com.esprit.pidev.entity.Category;


import java.util.List;

/**
 *
 * @author Djoo
 */
public interface IserviceCategory {
    void insert(Category c);
    void delete(int id);
     void update(Category c,int id);
    List<Category> displayall();
}
