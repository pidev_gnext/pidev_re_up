/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.esprit.pidev.gui;

import com.jfoenix.controls.JFXButton;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.RadioButton;
import javafx.scene.control.ToggleGroup;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;
 import com.esprit.pidev.entity.Partenaire;
import com.esprit.pidev.entity.Patient;
import com.esprit.pidev.entity.Doctor;
import com.esprit.pidev.service.DoctorService;
import com.esprit.pidev.service.PartenaireService;
import com.esprit.pidev.service.PatientService;
import com.jfoenix.controls.JFXPasswordField;
import static java.lang.Integer.parseInt;
import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
 import java.util.Date;
import javafx.scene.control.DatePicker;
import com.jfoenix.controls.JFXTextField;
import com.esprit.pidev.service.UserService;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.stage.Stage;
/**
 * FXML Controller class
 *
 * @author rohben
 */
public class InscriptionController implements Initializable {
    
    @FXML
    private Pane inscr_pane;
    @FXML
    private RadioButton pat;
    @FXML
    private ToggleGroup rolee;
    @FXML
    private RadioButton part;
    @FXML
    private RadioButton doc;
    @FXML
    private Pane iscr_pat;
    @FXML
    private Pane iscr_doct;
    @FXML
    private Pane iscr_part;
    @FXML
    private JFXButton nextt;
    @FXML
    private JFXTextField  nom,prenom,email,adr,cnam,matricule,societe_nom,website,phone,cin,specialite,cnx_email;
    @FXML
    private JFXPasswordField pwd,cnx_pwd;
    @FXML 
    private DatePicker Ndate;
    
    @FXML
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    
    

    @FXML
    private void nexti(MouseEvent event) {
        
          if (rolee.getSelectedToggle() != null) {

RadioButton chk = (RadioButton)part.getToggleGroup().getSelectedToggle();
              System.err.println(chk.getText());
        if(chk.getText().equals( "Doctor"))
        { 
            inscr_pane.setVisible(false);
            iscr_doct.setVisible(true);
            
        }
           if(chk.getText().equals( "Patient"))
        { 
            inscr_pane.setVisible(false);
            iscr_pat.setVisible(true);
            
        }
        if(chk.getText().equals( "Partenaire"))
        { 
            inscr_pane.setVisible(false);
            iscr_part.setVisible(true);
            
        }
           
// Do something here with the userData of newly selected radioButton

         }
    }
    @FXML
    private void inscription(){
        System.out.println(nom.getText()); 
        System.out.println(prenom.getText()); 
        System.out.println(email.getText()); 
        System.out.println(pwd.getText()); 
        System.out.println(cnam.getText()); 
        System.out.println(adr.getText()); 
         
            int nphone=parseInt(phone.getText());
      LocalDate d=    Ndate.getValue();
        Instant instant = Instant.from(d.atStartOfDay(ZoneId.systemDefault()));
        Date nd = Date.from(instant);
       
   
            java.sql.Date dn=new java.sql.Date(nd.getTime());
                  ///System.out.println(dn);
      
       RadioButton chk = (RadioButton)part.getToggleGroup().getSelectedToggle();
                     System.err.println(chk.getText());

        if(chk.getText().equals( "Doctor"))
        {   DoctorService ds=new DoctorService();
                        Doctor doctor;
                        System.out.println(cnam.getText());
                        System.out.println(matricule.getText());
         doctor=new Doctor(cnam.getText(), matricule.getText(), -1,3,adr.getText(),dn, email.getText(),false,prenom.getText() ,-1,nom.getText(),pwd.getText(),nphone,"test photo.jpg",-1);
                  System.out.println(doctor);

                   boolean ins=ds.insert(doctor);
        System.out.println(ins);
            
        }
           if(chk.getText().equals( "Patient"))
        {   PatientService ps=new PatientService();
            Patient p;
            int ncin=parseInt(cin.getText());
            p=new Patient(cnam.getText(),ncin,-1,adr.getText(),dn, email.getText(),false,prenom.getText() ,-1,nom.getText(),pwd.getText(),nphone,"test photo.jpg",-1);
                   System.out.println(p);

                   boolean ins=ps.insert(p);
        System.out.println(ins);
            
        }
        if(chk.getText().equals( "Partenaire"))
        { 
            PartenaireService ps=new PartenaireService();
            Partenaire p;
           
            p=new Partenaire(website.getText(), societe_nom.getText(), "logo.png",-1,adr.getText(),dn, email.getText(),false,prenom.getText() ,-1,nom.getText(),pwd.getText(),nphone,"test photo.jpg",-1);
                   System.out.println(p);

                   boolean ins=ps.insert(p);
        System.out.println(ins);
            
        }
        
     
    }
    @FXML
    private void logMe(ActionEvent event) throws IOException{
        UserService us=new UserService();
       int role= us.role(cnx_email.getText(),cnx_pwd.getText());
       switch(role){
           case 1:{
               PatientService ps=new PatientService();
               Patient.p=ps.get(cnx_email.getText(),cnx_pwd.getText());
               //System.out.println(p);
                 if(Patient.p!=null){
               try {
                Scene scene = new Scene(FXMLLoader.load(getClass().getResource("Patient.fxml")));
 
                Stage stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
               
                stage.setScene(scene);
                stage.show();
            } catch (IOException ex) {
                Logger.getLogger(HomeLoginController.class.getName()).log(Level.SEVERE, null, ex);
            }
                 }
           }break;
           case 2:{System.out.println("doctor");
               DoctorService ds=new DoctorService();
               Doctor d=ds.get(cnx_email.getText(),cnx_pwd.getText());
                              System.out.println(d);

                 if(d!=null)
               try {
                Scene scene = new Scene(FXMLLoader.load(getClass().getResource("Doctor.fxml")));
 
                Stage stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
               
                stage.setScene(scene);
                stage.show();
            } catch (IOException ex) {
                Logger.getLogger(HomeLoginController.class.getName()).log(Level.SEVERE, null, ex);
            }
           }break;
           case 3:{ System.out.println("part");
              PartenaireService prs=new PartenaireService();
              
              Partenaire par=prs.get(cnx_email.getText(),cnx_pwd.getText());
              if(par!=null)
               try {
                Scene scene = new Scene(FXMLLoader.load(getClass().getResource("Partenaire.fxml")));
 
                Stage stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
               
                stage.setScene(scene);
                stage.show();
            } catch (IOException ex) {
                Logger.getLogger(HomeLoginController.class.getName()).log(Level.SEVERE, null, ex);
            }

              
                  

           }break;
           case 0:{
               System.out.println("admin");
               
            }break;
           default :{
               System.err.println("not found");
           }break;
       }
        
    }
}
