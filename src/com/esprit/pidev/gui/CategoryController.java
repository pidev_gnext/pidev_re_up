/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.esprit.pidev.gui;

import com.esprit.pidev.entity.Category;
import com.esprit.pidev.service.categoryService;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TabPane;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;

/**
 * FXML Controller class
 *
 * @author rohben
 */
public class CategoryController implements Initializable {

    @FXML
    private TabPane ListCategory;
    @FXML
    private TextField nom;
    @FXML
    private TextArea description;
    @FXML
    private TableView<?> ListCat;
    @FXML
    private TableColumn<?, ?> id_c;
    private TableColumn<?, ?> desc;
    @FXML
    private TableColumn<?, ?> nomm;
    @FXML
    private TableColumn<?, ?> descc;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
       
        
          categoryService pscv = new categoryService();
        ArrayList arraylist = (ArrayList) pscv.displayall();
        ObservableList obs = FXCollections.observableArrayList(arraylist);
        ListCat.setItems(obs);
        System.out.print(arraylist.toString());
            id_c.setCellValueFactory(new PropertyValueFactory<>("id"));
           nomm.setCellValueFactory(new PropertyValueFactory<>("title"));
        descc.setCellValueFactory(new PropertyValueFactory<>("description"));
    }    

    @FXML
    private void addCategory(MouseEvent event) {
         Category p = new Category(nom.getText(),description.getText());
        categoryService ps = new categoryService();
        ps.insert(p);
    }
    
}
