/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.esprit.pidev.util;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 *
 * @author Manel
 */
public class DataSource {
        String url="jdbc:mysql://localhost:3306/pi_dev_beta1";
    String login="root";
    String pwd="";
    static Connection cnx =null;

    private DataSource() {
        try {
            cnx = DriverManager.getConnection(url, login, pwd);
            System.out.println("Connexion établie");
        } catch (SQLException ex) {
            System.out.println("Connexion non établie");
        }
    }
    
    public static Connection getInstance(){
        if(cnx==null){
            new DataSource();
        }
        return cnx;
    }
}
