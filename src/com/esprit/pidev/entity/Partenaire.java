package com.esprit.pidev.entity;

import java.sql.Date;


/**
 * @author aymen
 * @version 1.0
 * @created 26-mars-2018 20:16:17
 */
public class Partenaire extends User{

	protected int _id;
	protected String _logo;
	protected String _siteweb;
	protected String _societyName;
	protected int _user_id;
	//private Article m_Article;
	//private Deal m_Deal;
	//private User m_User;



	public void finalize() throws Throwable {

	}

	public Partenaire(){

	}

	
	public Partenaire(String siteweb, String societyName, String logo, int user_id,String _address, Date _createdAt ,String _email, boolean _etat, String _first_name, int _id, String _last_name, String _password, int _phoneNumber, String _photo, int _role){
            super(_first_name, _last_name,  _email, _password,_address,_photo, _etat,_phoneNumber,_createdAt,_role);
            this._siteweb=siteweb;
            this._societyName=societyName;
            this._logo=logo;
            this._user_id=user_id;
	}
/*
	public Article getArticle(){
		return m_Article;
	}

	public Deal getDeal(){
		return m_Deal;
	}

	public User getUser(){
		return m_User;
	}
*/

    public int getId() {
        return _id;
    }

    public void setId(int _id) {
        this._id = _id;
    }

    public String getLogo() {
        return _logo;
    }

    public void setLogo(String _logo) {
        this._logo = _logo;
    }

    public String getSiteweb() {
        return _siteweb;
    }

    public void setSiteweb(String _siteweb) {
        this._siteweb = _siteweb;
    }

    public String getSocietyName() {
        return _societyName;
    }

    public void setSocietyName(String _societyName) {
        this._societyName = _societyName;
    }

    public int getUser_id() {
        return _user_id;
    }

    public void setUser_id(int _user_id) {
        this._user_id = _user_id;
    }
	

	
	

}