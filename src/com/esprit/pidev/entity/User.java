package com.esprit.pidev.entity;

import java.sql.Date;


/**
 * @author aymen
 * @version 1.0
 * @created 26-mars-2018 20:16:18
 */
public class User {
//first_name, _last_name,  _email, _password,_address,_photo, _etat,_phoneNumber,_createdAt,_role
	protected String _first_name;
 	protected String _last_name;
        protected String _email;
	protected String _password;
        protected String _address;
        protected String _photo;
	protected boolean _etat;	
	protected int _phoneNumber;
        protected Date _createdAt;
	protected int _role;
        protected int _id;



	/*public void finalize() throws Throwable {

	}

	public User(){

	}

	/**
	 * 
	 * @param first_name
	 * @param last_name
	 * @param email
	 * @param password
	 * @param address
	 * @param photo
	 * @param etat
	 * @param phoneNumber
	 * @param createdAt
	 * @param type_id
	 */
	

    public String getAddress() {
        return _address;
    }

    public void setAddress(String _address) {
        this._address = _address;
    }

    public Date getCreatedAt() {
        return _createdAt;
    }

    public void setCreatedAt(Date _createdAt) {
        this._createdAt = _createdAt;
    }

    public String getEmail() {
        return _email;
    }

    public void setEmail(String _email) {
        this._email = _email;
    }

    public boolean isEtat() {
        return _etat;
    }

    public void setEtat(boolean _etat) {
        this._etat = _etat;
    }

    public String getFirst_name() {
        return _first_name;
    }

    public void setFirst_name(String _first_name) {
        this._first_name = _first_name;
    }

    

    public String getLast_name() {
        return _last_name;
    }

    public void setLast_name(String _last_name) {
        this._last_name = _last_name;
    }

    public String getPassword() {
        return _password;
    }

    public void setPassword(String _password) {
        this._password = _password;
    }

    public int getPhoneNumber() {
        return _phoneNumber;
    }

    public void setPhoneNumber(int _phoneNumber) {
        this._phoneNumber = _phoneNumber;
    }

    public String getPhoto() {
        return _photo;
    }

    public void setPhoto(String _photo) {
        this._photo = _photo;
    }
    public User(int id){
        this._id=id;
        this._role=id;
       
    }
    
     public User(){
    }
    public User(String _first_name, String _last_name, String _email, String _password, String _address, String _photo, boolean _etat, int _phoneNumber, Date _createdAt, int _role) {
        this._first_name = _first_name;
        this._last_name = _last_name;
        this._email = _email;
        this._password = _password;
        this._address = _address;
        this._photo = _photo;
        this._etat = _etat;
        this._phoneNumber = _phoneNumber;
        this._createdAt = _createdAt;
        this._role = _role;
    }

    public int getId() {
        return _id;
    }

    @Override
    public String toString() {
        return "User{" + "_first_name=" + _first_name + ", _last_name=" + _last_name + ", _email=" + _email + ", _password=" + _password + ", _address=" + _address + ", _photo=" + _photo + ", _etat=" + _etat + ", _phoneNumber=" + _phoneNumber + ", _createdAt=" + _createdAt + ", _role=" + _role + ", _id=" + _id + '}';
    }

    public void setId(int _id) {
        this._id = _id;
    }

   

    public int getRole() {
        return _role;
    }

    public void setRole(int role) {
        this._role = role;
    }

   

	
	

	
}