package com.esprit.pidev.entity;

import java.sql.Date;

public class FichePatient {
	private int id;
	private String content;
	private Date date;
	private int patient_id;
	
	public FichePatient(String content, int patient_id) {
		super();
		this.content = content;
		this.patient_id = patient_id;
	}
	@Override
	public String toString() {
		return "FichePatient [id=" + id + ", content=" + content + ", date="
				+ date + ", patient_id=" + patient_id + "]";
	}
	public FichePatient() {
		super();
	}
	public FichePatient(int id, String content, int patient_id) {
		super();
		this.id = id;
		this.content = content;
		this.patient_id = patient_id;
	}
	public int getId() {
		return id;
	}
	public String getContent() {
		return content;
	}
	public Date getDate() {
		return date;
	}
	public int getPatient_id() {
		return patient_id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public void setPatient_id(int patient_id) {
		this.patient_id = patient_id;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	
}
