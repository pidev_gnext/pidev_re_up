package com.esprit.pidev.entity;


public class Categories {

	protected int categorie_id;
        protected String categorie_nom; 
        protected String categorie_desc;
        protected int categorie_parent;
        
    public Categories()
    { 
    }

    public Categories(String categorie_nom, String categorie_desc, int categorie_parent) {
        this.categorie_nom = categorie_nom;
        this.categorie_desc = categorie_desc;
        this.categorie_parent = categorie_parent;
    }

    
    public Categories(int categorie_id, String categorie_nom, String categorie_desc, int categorie_parent) {
        this.categorie_id = categorie_id;
        this.categorie_nom = categorie_nom;
        this.categorie_desc = categorie_desc;
        this.categorie_parent = categorie_parent;
    }

    public int getCategorie_id() {
        return categorie_id;
    }

    public void setCategorie_id(int categorie_id) {
        this.categorie_id = categorie_id;
    }

    public String getCategorie_nom() {
        return categorie_nom;
    }

    public void setCategorie_nom(String categorie_nom) {
        this.categorie_nom = categorie_nom;
    }

    public String getCategorie_desc() {
        return categorie_desc;
    }

    public void setCategorie_desc(String categorie_desc) {
        this.categorie_desc = categorie_desc;
    }

    public int getCategorie_parent() {
        return categorie_parent;
    }

    public void setCategorie_parent(int categorie_parent) {
        this.categorie_parent = categorie_parent;
    }

    @Override
    public String toString() {
        return "Categories{" + "categorie_id=" + categorie_id + ", categorie_nom=" + categorie_nom + ", categorie_desc=" + categorie_desc + ", categorie_parent=" + categorie_parent + '}';
    }

    
    
                                                                              
}