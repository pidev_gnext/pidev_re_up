package com.esprit.pidev.entity;

public class Imc {

	private int id;
	private String sexe;
	private float taille;
	private float poid;
	private int age;
	private float resultat;
	private int patient_id;
	public int getId() {
		return id;
	}
	public String getSexe() {
		return sexe;
	}
	public float getTaille() {
		return taille;
	}
	public float getPoid() {
		return poid;
	}
	public int getAge() {
		return age;
	}
	public float getResultat() {
		return resultat;
	}
	public int getPatient_id() {
		return patient_id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public void setSexe(String sexe) {
		this.sexe = sexe;
	}
	public void setTaille(float taille) {
		this.taille = taille;
	}
	public void setPoid(float poid) {
		this.poid = poid;
	}
	public void setAge(int age) {
		this.age = age;
	}
	public void setResultat(float resultat) {
		this.resultat = resultat;
	}
	public void setPatient_id(int patient_id) {
		this.patient_id = patient_id;
	}
	@Override
	public String toString() {
		return "Imc [id=" + id + ", sexe=" + sexe + ", taille=" + taille
				+ ", poid=" + poid + ", age=" + age + ", resultat=" + resultat
				+ ", patient_id=" + patient_id + "]";
	}
	public Imc() {
		super();
	}
	public Imc(String sexe, float taille, float poid, int age, float resultat,
			int patient_id) {
		super();
		this.sexe = sexe;
		this.taille = taille;
		this.poid = poid;
		this.age = age;
		this.resultat = resultat;
		this.patient_id = patient_id;
	}
	
}
