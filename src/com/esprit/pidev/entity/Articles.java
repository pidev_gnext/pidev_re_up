package com.esprit.pidev.entity;



public class Articles {

	 protected Categories categorie;
         protected int article_id;
         protected String article_author;
         protected String article_date;
         protected String article_content;
         protected String article_title;
         protected int article_status;
         protected String article_modified;
         protected int categorie_id;
         
	
	public Articles(){ }

    public Articles(String article_author, String article_date, String article_content, String article_title, int article_status, String article_modified, int categorie_id) {
        this.article_author = article_author;
        this.article_date = article_date;
        this.article_content = article_content;
        this.article_title = article_title;
        this.article_status = article_status;
        this.article_modified = article_modified;
        this.categorie_id = categorie_id;
    }
        

    public Articles(String article_content) {
      
        this.article_content = article_content;
    }
        



    public int getArticle_id() {
        return article_id;
    }

    public void setArticle_id(int article_id) {
        this.article_id = article_id;
    }
    
    public Categories getCategorie() {
        return categorie;
    }

    public void setCategorie(Categories categorie) {
        this.categorie = categorie;
    }

    public String getArticle_author() {
        return article_author;
    }

    public void setArticle_author(String article_author) {
        this.article_author = article_author;
    }

 

    public String getArticle_content() {
        return article_content;
    }

    public void setArticle_content(String article_content) {
        this.article_content = article_content;
    }

    public String getArticle_title() {
        return article_title;
    }

    public void setArticle_title(String article_title) {
        this.article_title = article_title;
    }

    public int getArticle_status() {
        return article_status;
    }

    public void setArticle_status(int article_status) {
        this.article_status = article_status;
    }

    public String getArticle_date() {
        return article_date;
    }

    public void setArticle_date(String article_date) {
        this.article_date = article_date;
    }

    public String getArticle_modified() {
        return article_modified;
    }

    public void setArticle_modified(String article_modified) {
        this.article_modified = article_modified;
    }

    public int getCategorie_id() {
        return categorie_id;
    }

    public void setCategorie_id(int categorie_id) {
        this.categorie_id = categorie_id;
    }

    @Override
    public String toString() {
        return "Articles{" + "article_content=" + article_content + ", article_title=" + article_title + '}';
    }
    
   
    

    
}