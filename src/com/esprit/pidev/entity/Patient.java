package com.esprit.pidev.entity;

import java.sql.Date;


/**
 * @author aymen
 * @version 1.0
 * @created 26-mars-2018 20:16:18
 */
public class Patient extends User {
        public static Patient p;
	protected int _cin;
	protected String _cnam;
	protected int _id;
	protected int _user_id;
	//private Dealpatientparticipant m_Dealpatientparticipant;
	private User m_User;

    public Patient() {
        super();
    }

    @Override
    public String toString() {
        
        return super.toString()+"Patient{" + "_cin=" + _cin + ", _cnam=" + _cnam + ", _id=" + _id + ", _user_id=" + _user_id+'}';
    }



	
	

	public Patient(String cnam, int cin, int user_id,String _address, Date _createdAt ,String _email, boolean _etat, String _first_name, int _id, String _last_name, String _password, int _phoneNumber, String _photo, int _role){
            super(_first_name, _last_name,  _email, _password,_address,_photo, _etat,_phoneNumber,_createdAt,_role);
            this._cnam=cnam;
            this._cin= cin; 
            this._user_id=-1;
            this._id=-1;

	}

	public int getCin(){
		return this._cin;
	}

	public String getCnam(){
		return _cnam;
	}

	/*public Dealpatientparticipant getDealpatientparticipant(){
		return m_Dealpatientparticipant;
	}
*/
	public User getUser(){
		return m_User;
	}

	public int getId(){
		return _id;
	}

	/**
	 * 
	 * @param newVal
	 */
	/*public void setDealpatientparticipant(Dealpatientparticipant newVal){
		m_Dealpatientparticipant = newVal;
	}*/

	/**
	 * 
	 * @param newVal
	 */
	public void setUser(User newVal){
		m_User = newVal;
	}

	public int getUser_id(){
		return _user_id;
	}
         public Patient(int id){
        this._id=id;
    }
}