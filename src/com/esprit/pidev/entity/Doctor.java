package com.esprit.pidev.entity;

import java.sql.Date;


/**
 * @author aymen
 * @version 1.0
 * @created 26-mars-2018 20:16:17
 */
public class Doctor extends User{

	protected String _cnam;
	protected int _id;
	protected String _matricule;
	protected int _speciality_id;
	protected int _user_id;
	private User m_User;
//	private Doctorspeciality m_Doctorspeciality;

        
        

	 
	public Doctor(String matricule, String cnam, int user_id, int speciality_id,String _address, Date _createdAt ,String _email, boolean _etat, String _first_name, int _id, String _last_name, String _password, int _phoneNumber, String _photo, int _role){
            super(_first_name, _last_name,  _email, _password,_address,_photo, _etat,_phoneNumber,_createdAt,_role);
            this._cnam=cnam;
             this._user_id=-1;
             this._matricule=matricule;
             this._speciality_id=speciality_id;
           
	}

    public Doctor() {
     }

    public String getCnam() {
        return _cnam;
    }

    public void setCnam(String _cnam) {
        this._cnam = _cnam;
    }

    public int getId() {
        return _id;
    }

    public void setId(int _id) {
        this._id = _id;
    }

    public String getMatricule() {
        return _matricule;
    }

    public void setMatricule(String _matricule) {
        this._matricule = _matricule;
    }

    public int getSpeciality_id() {
        return _speciality_id;
    }

    public void setSpeciality_id(int _speciality_id) {
        this._speciality_id = _speciality_id;
    }

    public int getUser_id() {
        return _user_id;
    }

    public void setUser_id(int _user_id) {
        this._user_id = _user_id;
    }

    public User getM_User() {
        return m_User;
    }

    public void setM_User(User m_User) {
        this.m_User = m_User;
    }

   /* public Doctorspeciality getM_Doctorspeciality() {
        return m_Doctorspeciality;
    }

    public void setM_Doctorspeciality(Doctorspeciality m_Doctorspeciality) {
        this.m_Doctorspeciality = m_Doctorspeciality;
    }
*/


	
	

}