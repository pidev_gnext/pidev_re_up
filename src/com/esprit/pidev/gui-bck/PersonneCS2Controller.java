/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.esprit.pidev.gui;

import com.esprit.pidev.entity.PersonneCS;
import com.esprit.pidev.service.PersonneServiceCS;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;

/**
 * FXML Controller class
 *
 * @author Manel
 */
public class PersonneCS2Controller implements Initializable {

    @FXML
    private TextField tfnom;
    @FXML
    private TextField tfprenom;
    @FXML
    private Button btnajouter;
    @FXML
    private TableView<?> tableview;
    @FXML
    private TableColumn<?, ?> nom;
    @FXML
    private TableColumn<?, ?> prenom;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        PersonneServiceCS pscv = new PersonneServiceCS();
        ArrayList arraylist = (ArrayList) pscv.afficherPersonnes();
        ObservableList obs = FXCollections.observableArrayList(arraylist);
        tableview.setItems(obs);
        nom.setCellValueFactory(new PropertyValueFactory<>("nom"));
        prenom.setCellValueFactory(new PropertyValueFactory<>("prenom"));
    }    

    @FXML
    private void ajouterPersonne(ActionEvent event) {
        PersonneServiceCS psc = new PersonneServiceCS();
        PersonneCS p = new PersonneCS(tfnom.getText(), tfprenom.getText());
        psc.insertPersonne(p);
        
    }
    
}
