package com.esprit.pidev.gui;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author aymen
 */
public class SignupUsersController implements Initializable {

    /**
     * Initializes the controller class.
     * @param url
     * @param rb
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    } 
    @FXML
    private void selectRole(ActionEvent event) {
       /* PersonneServiceCS psc = new PersonneServiceCS();
        PersonneCS p = new PersonneCS(tfnom.getText(), tfprenom.getText());
        psc.insertPersonne(p);*/
       Node node = (Node) event.getSource() ;
    String data = (String) node.getUserData();
    int value = Integer.parseInt(data);
       switch(value){
           case 1:{
               try {
                Scene scene = new Scene(FXMLLoader.load(getClass().getResource("SignupPatient.fxml")));
 
                Stage stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
               
                stage.setScene(scene);
                stage.show();
            } catch (IOException ex) {
                Logger.getLogger(HomeLoginController.class.getName()).log(Level.SEVERE, null, ex);
            }

           }    break;
           case 2:{ 
               try {
                Scene scene = new Scene(FXMLLoader.load(getClass().getResource("SignupDortor.fxml")));
 
                Stage stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
               
                stage.setScene(scene);
                stage.show();
                } catch (IOException ex) {
                Logger.getLogger(HomeLoginController.class.getName()).log(Level.SEVERE, null, ex);
               }
            }   break;
           case 3:{
                 try {
                Scene scene = new Scene(FXMLLoader.load(getClass().getResource("SignupPartner.fxml")));
 
                Stage stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
               
                stage.setScene(scene);
                stage.show();
            } catch (IOException ex) {
                Logger.getLogger(HomeLoginController.class.getName()).log(Level.SEVERE, null, ex);
            }

           }break;
           }
       }
    }

